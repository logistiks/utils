import { Store } from 'confidence';

let manifest = new Store({
  sereurConfig: {
    server: {
      $filter: {
        $env: 'NODE_ENV',
      },
      $default: {
        host: 'localhost',
        port: {
          $env: 'PORT',
          $coerce: 'number',
          $default: 3000,
        },
        routes: {},
      },
      staging: {
        host: 'localhost',
        port: {
          $env: 'PORT',
          $coerce: 'number',
          $default: 4000,
        },
        routes: {},
      },
      production: {
        host: 'localhost',
        port: {
          $env: 'PORT',
          $coerce: 'number',
          $default: 5000,
        },
        routes: {},
      }
    },
    register: {
      $filter: {
        $env: 'NODE_ENV',
      },
      $base: {
        plugins: [],
        options: {
          once: true,
        }
      },
      $default: {
        plugins: [],
      },
      staging: {
        plugins: [],
      },
      production: {
        plugins: [],
      }
    },
  }
});

export { manifest };