import { Logger } from './logger';
import { manifest } from "./config";

export { Logger, manifest };