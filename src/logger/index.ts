// import * as Dotenv from 'dotenv';
import { Logger, format, transports, createLogger, level } from 'winston';

// Dotenv.config();

class ApiLogger {
  public static newInstance(): Logger {
    const consoleTransport = new transports.Console({
      format: format.combine(
        format.colorize(),
        format.timestamp(),
        format.align(),
        format.printf(info => {
          const { timestamp, level, message, ...args } = info;

          const ts = timestamp.slice(0, 19).replace('T', ' ');
          return `${ts} [${level}]: ${message} ${
            Object.keys(args).length ? JSON.stringify(args, null, 2) : ''
          }`;
        })
      ),
      level: (process.env.NODE_ENV === 'production') ? 'info' : 'debug',
    });

    return createLogger({
      transports: [consoleTransport],
    });
  }
}

let logger: Logger = ApiLogger.newInstance();

export { logger as Logger };